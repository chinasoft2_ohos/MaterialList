package com.dexafree.materialList.listeners;

/**
 * ComponentConfiguration
 *
 * @author hw
 * @since 2021/5/26
 */
public class ComponentConfiguration {
    private static final int DEFAULT_LONG_PRESS_TIMEOUT = 500;

    private static final int TOUCH_SLOP = 8;

    private static final int TAP_TIMEOUT = 100;

    private static final int DOUBLE_TAP_TIMEOUT = 300;

    private static final int DOUBLE_TAP_MIN_TIME = 40;

    private static final int DOUBLE_TAP_SLOP = 100;

    private static final int MINIMUM_FLING_VELOCITY = 50;

    private static final int MAXIMUM_FLING_VELOCITY = 8000;

    private static final int DOUBLE_TAP_TOUCH_SLOP = TOUCH_SLOP;

    private static final float AMBIGUOUS_GESTURE_MULTIPLIER = 2f;

    /**
     * getDoubleTapTimeout
     *
     * @return 双击超时
     */
    public static int getDoubleTapTimeout() {
        return DOUBLE_TAP_TIMEOUT;
    }

    /**
     * getDoubleTapMinTime
     *
     * @return 最小时间
     */
    public static int getDoubleTapMinTime() {
        return DOUBLE_TAP_MIN_TIME;
    }

    /**
     * getLongPressTimeout
     *
     * @return 长按超时
     */
    public static int getLongPressTimeout() {
        return DEFAULT_LONG_PRESS_TIMEOUT;
    }

    /**
     * getTapTimeout
     *
     * @return getTapTimeout
     */
    public static int getTapTimeout() {
        return TAP_TIMEOUT;
    }

    /**
     * getScaledTouchSlop
     *
     * @return getScaledTouchSlop
     */
    public static int getScaledTouchSlop() {
        return TOUCH_SLOP;
    }

    /**
     * getScaledDoubleTapTouchSlop
     *
     * @return getScaledDoubleTapTouchSlop
     */
    public static int getScaledDoubleTapTouchSlop() {
        return DOUBLE_TAP_TOUCH_SLOP;
    }

    /**
     * getScaledDoubleTapSlop
     *
     * @return getScaledDoubleTapSlop
     */
    public static int getScaledDoubleTapSlop() {
        return DOUBLE_TAP_SLOP;
    }

    /**
     * getScaledMinimumFlingVelocity
     *
     * @return 滑动最小速率
     */
    public static int getScaledMinimumFlingVelocity() {
        return MINIMUM_FLING_VELOCITY;
    }

    /**
     * getScaledMaximumFlingVelocity
     *
     * @return 滑动最大速率
     */
    public static int getScaledMaximumFlingVelocity() {
        return MAXIMUM_FLING_VELOCITY;
    }

    /**
     * getAmbiguousGestureMultiplier
     *
     * @return getAmbiguousGestureMultiplier
     */
    public static float getAmbiguousGestureMultiplier() {
        return AMBIGUOUS_GESTURE_MULTIPLIER;
    }
}
