package com.dexafree.materialList.listeners;

import com.dexafree.materialList.card.Card;

/**
 * The OnDismissCallback will be notified if a Card is dismissed.
 *
 * @author hw
 * @since 2021/5/26
 */
public interface OnDismissCallback {
    /**
     * A Card is dismissed.
     *
     * @param card     which is dismissed.
     * @param position where the Card is.
     */
    void onDismiss(final Card card, int position);
}
