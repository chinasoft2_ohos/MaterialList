package com.dexafree.materialList.card.event;

import com.dexafree.materialList.card.Card;

/**
 * DismissEvent
 *
 * @author hw
 * @since 2021/5/26
 */
public class DismissEvent {
    private final Card mCard;

    public DismissEvent(final Card card) {
        mCard = card;
    }

    public Card getCard() {
        return mCard;
    }
}
