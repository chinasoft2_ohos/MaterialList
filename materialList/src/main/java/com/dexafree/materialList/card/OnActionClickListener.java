package com.dexafree.materialList.card;

import ohos.agp.components.Component;

/**
 * The OnActionClickListener will be notified if a Button is clicked on the Card.
 *
 * @author hw
 * @since 2021/5/26
 */
public interface OnActionClickListener {
    /**
     * An action view is clicked.
     *
     * @param view which was clicked.
     * @param card where the view was clicked on.
     */
    void onActionClicked(Component view, Card card);
}
