package com.dexafree.materialList.card.action;

import com.dexafree.materialList.card.Card;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

/**
 * WelcomeButtonAction
 *
 * @author hw
 * @since 2021/5/26
 */
public class WelcomeButtonAction extends TextViewAction {
    public WelcomeButtonAction(Context context) {
        super(context);
    }

    @Override
    protected void onRender(Component view, Card card) {
        super.onRender(view, card);
        final Text button = (Text) view;
        Element drawable = button.getAroundElements()[0];
        drawable.setBounds(0, 0, 50, drawable.getHeight());
        button.setAroundElements(drawable, null, null, null);
    }
}
