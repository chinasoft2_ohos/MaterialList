package com.dexafree.materialList.view.cardview;

import com.dexafree.materialList.AttrUtils;
import ohos.agp.components.*;
import ohos.agp.render.BlendMode;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * CardViewOhos
 *
 * @author hw
 * @since 2021/5/26
 */
public class CardViewOhos extends StackLayout implements Component.DrawTask, ComponentContainer.ArrangeListener {
    final Rect mContentPadding;
    final Rect mShadowBounds;
    private final RectFloat roundRect = new RectFloat();
    private final Paint maskPaint = new Paint();
    private final Paint zonePaint = new Paint();
    private final Paint strokePaint = new Paint();
    private float mStrokeWidth = 0;
    private float roundCorner = 50;
    private boolean mCompatPadding;
    int mUserSetMinWidth;
    int mUserSetMinHeight;

    public CardViewOhos(Context context) {
        this(context, (AttrSet) null);
    }

    public CardViewOhos(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public CardViewOhos(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        this.mContentPadding = new Rect();
        this.mShadowBounds = new Rect();
        this.mCompatPadding = AttrUtils.getBoolean(attrs, "mCompatPadding", false);
        int defaultPadding = AttrUtils.getInteger(attrs, "defaultPadding", 5);
        this.mContentPadding.left = AttrUtils.getInteger(attrs, "leftPadding", defaultPadding);
        this.mContentPadding.top = AttrUtils.getInteger(attrs, "topPadding", defaultPadding);
        this.mContentPadding.right = AttrUtils.getInteger(attrs, "rightPadding", defaultPadding);
        this.mContentPadding.bottom = AttrUtils.getInteger(attrs, "bottomPadding", defaultPadding);
        this.mUserSetMinWidth = AttrUtils.getInteger(attrs, "minWidth", 0);
        this.mUserSetMinHeight = AttrUtils.getInteger(attrs, "minHeight", 0);
        maskPaint.setAntiAlias(true);
        maskPaint.setBlendMode(BlendMode.DIFFERENCE);
        maskPaint.setStyle(Paint.Style.FILL_STYLE);
        maskPaint.setColor(Color.GREEN);
        zonePaint.setAntiAlias(true);
        zonePaint.setColor(Color.YELLOW);
        strokePaint.setAntiAlias(true);
        strokePaint.setColor(Color.BLUE);
        strokePaint.setStrokeWidth(10);
    }

    /**
     * getUseCompatPadding
     *
     * @return boolean
     */
    public boolean getUseCompatPadding() {
        return this.mCompatPadding;
    }

    /**
     * 设置最小宽度
     *
     * @param minWidth 宽度
     */
    public void setMinimumWidth(int minWidth) {
        this.mUserSetMinWidth = minWidth;
        super.setMinWidth(minWidth);
    }

    /**
     * 设置最小高度
     *
     * @param minHeight
     */
    public void setMinimumHeight(int minHeight) {
        this.mUserSetMinHeight = minHeight;
        super.setMinHeight(minHeight);
    }

    @Override
    public void addDrawTask(DrawTask task) {
        super.addDrawTask(task);
        task.onDraw(this, mCanvasForTaskUnderContent);
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (roundCorner > 0) {
            canvas.saveLayer(roundRect, zonePaint);
            canvas.drawRoundRect(roundRect, roundCorner, roundCorner, zonePaint);
            canvas.saveLayer(roundRect, maskPaint);
            canvas.restore();
            RectFloat roundRect2 = new RectFloat();
            roundRect2.fuse((float) (roundRect.left + (double)mStrokeWidth / 2), (float) (roundRect.top + (double)mStrokeWidth / 2),
                    (float) (roundRect.right - (double)mStrokeWidth / 2), (float) (roundRect.bottom - (double)mStrokeWidth / 2));
            canvas.drawRoundRect(roundRect2, roundCorner, roundCorner, strokePaint);
        }
    }

    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        int width = getWidth();
        int height = getHeight();
        roundRect.fuse(0, 0, width, height);
        return false;
    }
}
