package com.dexafree.materialList;

import ohos.agp.components.AttrSet;

/**
 * AttrUtils
 * 自定义属性工具类
 * 注意： 宽高都为 match_content 且无实际内容时构造方法不会调用
 * 使用方法：
 * xxx extends Component
 * 获取自定义属性：
 * String count = AttrUtils.getStringFromAttr(attrSet,"cus_count","0");
 * 属性定义：
 * 布局头中加入  xmlns:hap="http://schemas.huawei.com/apk/res/ohos" 使用hap区分自定义属性与系统属性
 * 即可使用 hap:cus_count="2"  不加直接使用ohos:cus_count="2"
 *
 * @author hw
 * @since 2021/5/26
 */
public class AttrUtils {
    /**
     * 获取String
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认值
     * @return string值
     */
    public static String getString(AttrSet attrSet, String name, String defaultValue) {
        String value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getStringValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 获取int值
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认值
     * @return int值
     */
    public static Integer getInteger(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 获取float值
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认值
     * @return float值
     */
    public static float getFloat(AttrSet attrSet, String name, float defaultValue) {
        float value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getFloatValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * boolean属性值
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认值
     * @return boolean
     */
    public static boolean getBoolean(AttrSet attrSet, String name, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 获取long值
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认值
     * @return long值
     */
    public static Long getLong(AttrSet attrSet, String name, Long defaultValue) {
        Long value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getLongValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Color.WHITE.getValue()
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认值
     * @return 色值
     */
    public static int getColor(AttrSet attrSet, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

}
