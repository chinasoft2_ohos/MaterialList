package com.dexafree.materiallistviewexample;

import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;

/**
 *  MyPopuDialog 自定义的菜单
 *
 * @author hw
 * @since 2021/5/26
 */
public class MyPopuDialog extends PopupDialog {
    private final Component.ClickedListener clickedListener;

    public MyPopuDialog(Context context, Component contentComponent, Component.ClickedListener clickedListener) {
        super(context, contentComponent);
        this.clickedListener = clickedListener;
        Component view = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_popu_dialog, null, true);
        findCommentById(view);
        setCustomComponent(view);
    }

    private void findCommentById(Component view) {
        if (view.findComponentById(ResourceTable.Id_tv_clear) instanceof Text) {
            Text tvClear = (Text) view.findComponentById(ResourceTable.Id_tv_clear);
            tvClear.setClickedListener(clickedListener);
        }
        if (view.findComponentById(ResourceTable.Id_tv_add_satrt) instanceof Text) {
            Text tvAddStart = (Text) view.findComponentById(ResourceTable.Id_tv_add_satrt);
            tvAddStart.setClickedListener(clickedListener);
        }
        if (view.findComponentById(ResourceTable.Id_tv_setting) instanceof Text) {
            Text tvSetting = (Text) view.findComponentById(ResourceTable.Id_tv_setting);
            tvSetting.setClickedListener(clickedListener);
        }
    }

}
