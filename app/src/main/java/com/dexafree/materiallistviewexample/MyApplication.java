package com.dexafree.materiallistviewexample;

import ohos.aafwk.ability.AbilityPackage;

/**
 *  MyApplication
 *
 * @author hw
 * @since 2021/5/26
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
