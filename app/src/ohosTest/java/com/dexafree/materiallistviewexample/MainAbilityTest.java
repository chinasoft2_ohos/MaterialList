package com.dexafree.materiallistviewexample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

/**
 * com.dexafree.materiallistviewexample
 *
 * @author hw
 * @since 2021/6/9
 */
public class MainAbilityTest {
    @Test
    public void testOnStart() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Assert.assertNotNull(context);
    }
}